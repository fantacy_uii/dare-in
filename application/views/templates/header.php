<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo $title; ?></title>

	<link href="<?php echo base_url().'assets_2/css/bootstrap.css'?>" rel="stylesheet">
	
	<style>
	.container-fluid
	{
		padding: 15px;
	}

	.control-label
	{
		text-align: left !important;
	}

	.form-input
	{
		max-width: 700px;
		margin: 0 auto;
	}

	.btn-form
	{
		text-align: right;
	}
</style>
</head>
<body> <style >
	body:before {
  content: "";
  position: fixed;
  z-index: -1;
  background-size:cover;
  background-position:center top;
  display: block;
  background-image: url("relawan.png");
  width: 100%;
  height: 100%;
  filter: blur(5px) ;
  -webkit-filter: blur(5px) ;
}
</style>
	<div class="container">
	<?php $this->load->view('Menu');?>