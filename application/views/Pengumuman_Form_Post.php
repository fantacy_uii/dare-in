<?php
	defined('BASEPATH') OR exit('Akses langsung tidak diperbolehkan');
	//echo validation_errors();
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets_3/css/bootstrap.css'?>">

<section class="container-fluid">
	<div class="row">
		<div class="form-input clearfix">
			<div class="col-md-12">
				
				<?php
					if(isset($_SESSION['input_sukses']))
					{
				?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  	<strong>Sukses!</strong> <?php echo $_SESSION['input_sukses']; ?>
						</div>
				<?php
					}
				?>

				<div class="panel panel-primary">
					<div class="panel-heading">Post Pengumuman</div>
					<div class="panel-body">
					
		<div class="col-md-8 col-md-offset-2">
			<form action="<?php echo base_url().'Pengumuman/post_pengumuman'?>" method="post" enctype="multipart/form-data">
	            <input type="text" name="judul_pengumuman" class="form-control" placeholder="Judul Pengumuman" required/><br/>
	            <input type="date" name="tanggal_pengumuman" class="form-control" placeholder="tanggal_pengumuman" required=""><br>
	            <textarea id="ckeditor" name="isi_pengumuman" class="form-control" required></textarea><br/>
	            <label>Upload Gambar :</label>
	            <input type="file" name="gambar_pengumuman" required><br>
	            <button class="btn btn-primary btn-lg" type="submit">Post Pengumuman</button>
            </form>
		</div>
	

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script src="<?php echo base_url().'assets_3/jquery/jquery-2.2.3.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets_3/js/bootstrap.js'?>"></script>
	<script src="<?php echo base_url().'assets_3/ckeditor/ckeditor.js'?>"></script>
	<script type="text/javascript">
	  $(function () {
	    CKEDITOR.replace('ckeditor');
	  });
	</script>