<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets_3/css/bootstrap.css'?>">
<link href="<?php echo base_url().'assets/login_source/login.css'?>" rel="stylesheet">

<section class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Pengumuman</div>
				<div class="panel-body">
					<div class="col-md-12" style="padding-bottom: 15px;">
						<a href="<?php echo base_url('Relawan_Page'); ?>">
							<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-back"></span> Home</button> 
						</a>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							
							<?php
			function limit_words($string, $word_limit){
                $words = explode(" ",$string);
                return implode(" ",array_splice($words,0,$word_limit));
            }
			foreach ($data->result_array() as $i) :
				$id_pengumuman=$i['id_pengumuman'];
				$judul_pengumuman=$i['judul_pengumuman'];
				$tanggal_pengumuman=$i['tanggal_pengumuman'];
				$isi_pengumuman=$i['isi_pengumuman'];
				$gambar_pengumuman=$i['gambar_pengumuman'];
				?>

				<div class="col-md-8 col-md-offset-2">
			<h2><?php echo $judul_pengumuman;?></h2><hr/>
			<img src="<?php echo base_url().'assets_3/images/'.$gambar_pengumuman;?>">
			<?php echo limit_words($isi_pengumuman,30);?><a href="<?php echo base_url().'Pengumuman/view_feed/'.$id_pengumuman;?>"> Selengkapnya ></a>
		</div>
		<?php endforeach;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="<?php echo base_url().'assets_3/jquery/jquery-2.2.3.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets_3/js/bootstrap.js'?>"></script>