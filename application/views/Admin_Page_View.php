<!DOCTYPE html>
<html>
  <head>
    <title>Masuk</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'assets_2/css/bootstrap.css'?>" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url().'assets/home_source/fontawesome-free/css/all.min.css'?>" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="<?php echo base_url().'assets/home_source/magnific-popup/magnific-popup.css'?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url().'assets/home_source/css/home.css'?>" rel="stylesheet">

    <link href="<?php echo base_url().'assets/login_source/css/Admin_Page.css'?>" rel="stylesheet">



  </head>
  <body>

    <div class="container">
      <?php $this->load->view('Menu');?> <!--Include menu-->
      <div class="container">
        <div class="row">
          
        </div>
      </div>
    </div> <!-- /container -->

    <center style="margin-top: 210px;">
    <section class="bg-primary" id="">
      <div class="container">
        <div class="row">
          
            <h2 class="section-heading text-white"><h2>Selamat Datang Admin, <?php echo $this->session->userdata('ses_nama');?></h2></h2>
        </div>
      </div>
    </section>
    </center>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>

  </body>
</html>