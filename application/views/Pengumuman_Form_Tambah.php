<!--
<?php
	defined('BASEPATH') OR exit('Akses langsung tidak diperbolehkan');
	//echo validation_errors();
?>

<section class="container-fluid">
	<div class="row">
		<div class="form-input clearfix">
			<div class="col-md-12">
				
				<?php
					if(isset($_SESSION['input_sukses']))
					{
				?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  	<strong>Sukses!</strong> <?php echo $_SESSION['input_sukses']; ?>
						</div>
				<?php
					}
				?>

				<div class="panel panel-primary">
					<div class="panel-heading">Tambah Data Pengumuman</div>
					<div class="panel-body">
						
						<?php echo form_open('Pengumuman/tambahpengumuman', ['class' => 'form-horizontal', 'method' => 'post']); ?>

							<div class="form-group">
								<label for="judul_pengumuman" class="control-label col-sm-2">Judul Pengumuman </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="judul_pengumuman" value="<?php echo set_value('judul_pengumuman'); ?>">
									<?php echo form_error('judul_pengumuman'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="tanggal_pengumuman" class="control-label col-sm-2">Tanggal Pengumuman </label>
								<div class="col-sm-10">
									<input type="date" class="form-control" name="tanggal_pengumuman" value="<?php echo set_value('tanggal_pengumuman'); ?>">
									<?php echo form_error('tanggal_pengumuman'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="isi_pengumuman" class="control-label col-sm-2">Isi Pengumuman </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="isi_pengumuman" value="<?php echo set_value('isi_pengumuman'); ?>">
									<?php echo form_error('isi_pengumuman'); ?>
								</div>
							</div>

							<div class="form-group">
								<div class="btn-form col-sm-12">
									<a href="<?php echo base_url('Pengumuman/Adminlihatdata'); ?>"><button type="button" class='btn btn-default'>Batal</button></a>
									<button type="submit" class='btn btn-primary'>Simpan</button>
								</div>
							</div>
						<?php echo form_close(); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>