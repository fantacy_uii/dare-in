<?php
	defined('BASEPATH') OR exit('Akses langsung tidak diperbolehkan');
	//echo validation_errors();
?>

<section class="container-fluid">
	<div class="row">
		<div class="form-input clearfix">
			<div class="col-md-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Edit Data Riwayat Aktivitas</div>
					<div class="panel-body">
						
						<?php echo form_open('Riwayat/updateriwayat/'.$db->id_riwayat, ['class' => 'form-horizontal', 'method' => 'post']); ?>
							<div class="form-group <?php echo (form_error('id_riwayat') != '') ? 'has-error has-feedback' : '' ?>">
								<label for="id_riwayat" class="control-label col-sm-2">ID Aktivitas </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="id_riwayat" value="<?php echo set_value('id_riwayat', $db->id_riwayat); ?>" readonly>
									<?php echo (form_error('id_riwayat') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
									<?php echo form_error('id_riwayat'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="nama_riwayat" class="control-label col-sm-2">Nama Aktivitas </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="nama_riwayat" value="<?php echo set_value('nama_riwayat', $db->nama_riwayat); ?>">
									<?php echo form_error('nama_riwayat'); ?>
								</div>
							</div>


							<div class="form-group">
								<label for="mulai_riwayat" class="control-label col-sm-2">Mulai Aktivitas </label>
								<div class="col-sm-10">
									<input type="date" class="form-control" name="mulai_riwayat" value="<?php echo set_value('mulai_riwayat', $db->mulai_riwayat); ?>">
									<?php echo form_error('mulai_riwayat'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="akhir_riwayat" class="control-label col-sm-2">Akhir Aktivitas </label>
								<div class="col-sm-10">
									<input type="date" class="form-control" name="akhir_riwayat" value="<?php echo set_value('akhir_riwayat', $db->akhir_riwayat); ?>">
									<?php echo form_error('akhir_riwayat'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id_relawan" class="control-label col-sm-2">ID Relawan </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="id_relawan" value="<?php echo set_value('id_relawan', $db->id_relawan); ?>">
									<?php echo form_error('id_relawan'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id_posko" class="control-label col-sm-2">ID Posko </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="id_posko" value="<?php echo set_value('id_posko', $db->id_posko); ?>">
									<?php echo form_error('id_posko'); ?>
								</div>
							</div>


							
							<div class="form-group">
								<div class="btn-form col-sm-12">
									<a href="<?php echo base_url('Riwayat/lihatdata'); ?>"><button type="button" class='btn btn-default'>Batal</button></a>
									<button type="submit" class='btn btn-primary'>Simpan</button>
								</div>
							</div>
						<?php echo form_close(); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>