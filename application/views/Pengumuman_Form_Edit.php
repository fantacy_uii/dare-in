<!--

<?php
	defined('BASEPATH') OR exit('Akses langsung tidak diperbolehkan');
	//echo validation_errors();
?>

<section class="container-fluid">
	<div class="row">
		<div class="form-input clearfix">
			<div class="col-md-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Edit Data Pengumuman</div>
					<div class="panel-body">
						
						<?php echo form_open('Pengumuman/updatepengumuman/'.$db->id_pengumuman, ['class' => 'form-horizontal', 'method' => 'post']); ?>
							<div class="form-group <?php echo (form_error('id_pengumuman') != '') ? 'has-error has-feedback' : '' ?>">
								<label for="id_pengumuman" class="control-label col-sm-2">ID Pengumuman </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="id_pengumuman" value="<?php echo set_value('id_pengumuman', $db->id_pengumuman); ?>" readonly>
									<?php echo (form_error('id_pengumuman') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
									<?php echo form_error('id_pengumuman'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="judul_pengumuman" class="control-label col-sm-2"> Nama Pengumuman </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="judul_pengumuman" value="<?php echo set_value('judul_pengumuman', $db->judul_pengumuman); ?>">
									<?php echo form_error('judul_pengumuman'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="tanggal_pengumuman" class="control-label col-sm-2">Tanggal Pengumuman </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="tanggal_pengumuman" value="<?php echo set_value('tanggal_pengumuman', $db->tanggal_pengumuman); ?>">
									<?php echo form_error('tanggal_pengumuman'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="isi_pengumuman" class="control-label col-sm-2"> Isi Pengumuman </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="isi_pengumuman" value="<?php echo set_value('isi_pengumuman', $db->isi_pengumuman); ?>">
									<?php echo form_error('isi_pengumuman'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="gambar_pengumuman" class="control-label col-sm-2"> Gambar </label>
								<div class="col-sm-10">
									<input type="file" name="gambar_pengumuman" value="<?php echo set_value('gambar_pengumuman', $db->gambar_pengumuman); ?>">
									<?php echo form_error('gambar_pengumuman'); ?>
								</div>
							</div>

							
							<div class="form-group">
								<div class="btn-form col-sm-12">
									<a href="<?php echo base_url('Pengumuman/Adminlihatdata'); ?>"><button type="button" class='btn btn-default'>Batal</button></a>
									<button type="submit" class='btn btn-primary'>Simpan</button>
								</div>
							</div>
						<?php echo form_close(); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>