<?php
	defined('BASEPATH') OR exit('Akses langsung tidak diperbolehkan');
	//echo validation_errors();
?>

<section class="container-fluid">
	<div class="row">
		<div class="form-input clearfix">
			<div class="col-md-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Edit Data Tim</div>
					<div class="panel-body">
						
						<?php echo form_open('Team/updatetim/'.$db->id_team, ['class' => 'form-horizontal', 'method' => 'post']); ?>

							<div class="form-group">
								<label for="id_team" class="control-label col-sm-2"> ID Team </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="id_team" value="<?php echo set_value('id_team', $db->id_team); ?>" readonly>
									<?php echo form_error('id_team'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="nama_team" class="control-label col-sm-2">Nama Team </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="nama_team" value="<?php echo set_value('nama_team', $db->nama_team); ?>">
									<?php echo form_error('nama_team'); ?>
								</div>
							</div>


							<div class="form-group">
								<label for="id_posko" class="control-label col-sm-2">ID Posko </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="id_posko" value="<?php echo set_value('id_posko', $db->id_posko); ?>">
									<?php echo form_error('id_posko'); ?>
								</div>
							</div>


							<div class="form-group">
								<label for="regional" class="control-label col-sm-2">Regional </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="regional" value="<?php echo set_value('regional', $db->regional); ?>">
									<?php echo form_error('regional'); ?>
								</div>
							</div>


							
							<div class="form-group">
								<div class="btn-form col-sm-12">
									<a href="<?php echo base_url('Team/lihatdata'); ?>"><button type="button" class='btn btn-default'>Batal</button></a>
									<button type="submit" class='btn btn-primary'>Simpan</button>
								</div>
							</div>
						<?php echo form_close(); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>