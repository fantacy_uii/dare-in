<?php
	defined('BASEPATH') OR exit('Akses langsung tidak diperbolehkan');
	//echo validation_errors();
?>

<section class="container-fluid">
	<div class="row">
		<div class="form-input clearfix">
			<div class="col-md-12">
				
				<?php
					if(isset($_SESSION['input_sukses']))
					{
				?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  	<strong>Sukses!</strong> <?php echo $_SESSION['input_sukses']; ?>
						</div>
				<?php
					}
				?>

				<div class="panel panel-primary">
					<div class="panel-heading">Tambah Data Relawan</div>
					<div class="panel-body">
						
						<?php echo form_open('Relawan/tambahrelawan', ['class' => 'form-horizontal', 'method' => 'post']); ?>
							<div class="form-group <?php echo (form_error('id_relawan') != '') ? 'has-error has-feedback' : '' ?>">
								
								<label for="username" class="control-label col-sm-2">Username </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="username" value="<?php echo set_value('username'); ?>">
									<?php echo (form_error('username') != '') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' ?>
									<?php echo form_error('username'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="password" class="control-label col-sm-2">Password </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="password" value="<?php echo set_value('password'); ?>">
									<?php md5('password');?>
									<?php echo form_error('password'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="NIK" class="control-label col-sm-2">NIK </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="NIK" value="<?php echo set_value('NIK'); ?>">
									<?php echo form_error('NIK'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="nama" class="control-label col-sm-2">Nama </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="nama" value="<?php echo set_value('nama'); ?>">
									<?php echo form_error('nama'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="tanggal_lahir" class="control-label col-sm-2">Tanggal Lahir </label>
								<div class="col-sm-10">
									<input type="date" class="form-control" name="tanggal_lahir" value="<?php echo set_value('tanggal_lahir'); ?>">
									<?php echo form_error('tanggal_lahir'); ?>
								</div>
							</div>



							<div class="form-group">
								<label for="jenis_kelamin" class="control-label col-sm-2">Jenis kelamin </label>
								<div class="col-sm-10">
									<select name="jenis_kelamin" id="" class="form-control">
										<option value="laki-laki">Laki - Laki</option>
										<option value="perempuan">Perempuan</option>
									</select>
									<?php echo form_error('jenis_kelamin'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="alamat" class="control-label col-sm-2">Alamat </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="alamat" value="<?php echo set_value('alamat'); ?>">
									<?php echo form_error('alamat'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="pekerjaan" class="control-label col-sm-2">Pekerjaan </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="pekerjaan" value="<?php echo set_value('pekerjaan'); ?>">
									<?php echo form_error('pekerjaan'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="jenis_relawan" class="control-label col-sm-2">Jenis Relawan </label>
								<div class="col-sm-10">
									<select name="jenis_relawan" id="" class="form-control">
										<option value="Dokter">Dokter</option>
										<option value="Epidemiologis">Epidemiologis</option>
										<option value="Surveilans">Survelians</option>
										<option value="Sanitarian">Sanitarian</option>
										<option value="Ahli Gizi Masyarakat">Ahli Gizi Masyarakat</option>
										<option value="Perawat">Perawat</option>
										<option value="Apoteker">Apoteker</option>
										<option value="Asisten Apoteker">Asisten Apoteker</option>
										<option value="Transporter">Transporter</option>
										<option value="Staf Komunikasi">Staf Komunikasi</option>
									</select>
									<?php echo form_error('jenis_relawan'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="keahlian" class="control-label col-sm-2">Keahlian </label>
								<div class="col-sm-10">
									<select name="keahlian" id="" class="form-control">
										<option value="-">-</option>
										<option value="Dokter-PPGD">Dokter-PPGD</option>
										<option value="Dokter-GELS">Dokter-GELS</option>
										<option value="Dokter-ATLS">Dokter-ATLS</option>
										<option value="Dokter-ACLS">Dokter-ACLS</option>
										<option value="Perawat-BTLS">Perawat-BTLS</option>
									</select>
									<?php echo form_error('keahlian'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id_team" class="control-label col-sm-2">ID Team </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="id_team" value="<?php echo set_value('id_team'); ?>">
									<?php echo form_error('id_team'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id_posko" class="control-label col-sm-2">ID Posko </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="id_posko" value="<?php echo set_value('id_posko'); ?>">
									<?php echo form_error('id_posko'); ?>
								</div>
							</div>



							<div class="form-group">
								<div class="btn-form col-sm-12">
									<a href="<?php echo base_url('Relawan/lihatdata'); ?>"><button type="button" class='btn btn-default'>Batal</button></a>
									<button type="submit" class='btn btn-primary'>Simpan</button>
								</div>
							</div>
						<?php echo form_close(); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>