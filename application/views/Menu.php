<nav class="navbar navbar-inverse">
<div class="container-fluid">
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav">
  <!--Akses Menu Untuk Admin-->
  <?php if($this->session->userdata('akses')=='1'):?>
      <li class="active"><a href="<?php echo base_url().'Admin_Page'?>">Portal Admin</a></li>
      <li><a href="<?php echo base_url().'Relawan/lihatdata'?>"> Relawan</a></li>
      <li><a href="<?php echo base_url().'Team/lihatdata'?>">Tim</a></li>
      <li><a href="<?php echo base_url().'Posko/lihatdata'?>"> Posko</a></li>
      <li><a href="<?php echo base_url().'Riwayat/lihatdata'?>">Aktifitas</a></li>
      <li><a href="<?php echo base_url().'Pengumuman/Adminlihatdata'?>">Pengumuman</a></li>

  <!--Akses Menu Untuk Relawan-->
  <?php else:?>
      <li class="active"><a href="<?php echo base_url().'Relawan_Page'?>">Portal Relawan</a></li>
       <li><a href="<?php echo base_url().'Relawan/profil'?>">Profil</a></li>
      <li><a href="<?php echo base_url().'Team/list'?>">Tim</a></li>
      <li><a href="<?php echo base_url().'Posko/list'?>">Posko</a></li>
      <li><a href="<?php echo base_url().'Pengumuman/feed'?>">Pengumuman</a></li>
  <?php endif;?>
  </ul>

  <ul class="nav navbar-nav navbar-right">
    <li><a href="<?php echo base_url().'Login/logout'?>">Sign Out</a></li>
  </ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
<link href="<?php echo base_url().'assets/login_source/login.css'?>" rel="stylesheet">
