<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="container-fluid">
	<div class="row">
		<div class="col-md-12">

			<?php
				if(isset($_SESSION['hapus_sukses']) || isset($_SESSION['update_sukses'])) :
					$notif = '';

					isset($_SESSION['hapus_sukses']) ? $notif .= $_SESSION['hapus_sukses'] : '';
					isset($_SESSION['update_sukses']) ? $notif .= $_SESSION['update_sukses'] : '';
			?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  	<strong>Sukses!</strong> <?php echo $notif; ?>
					</div>
			<?php
				endif;
			?>

			<div class="panel panel-primary">
				<div class="panel-heading">Data Pengumuman</div>
				<div class="panel-body">
					<div class="col-md-12" style="padding-bottom: 15px;">
						<a href="<?php echo base_url('Pengumuman/formpost'); ?>">
							<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Post Pengumuman</button> 
						</a>

						<a href="<?php echo base_url('Pengumuman/lists_pengumuman'); ?>">
							<button type="button" class="btn btn-primary"><span class=""></span> Lihat Pengumuman</button> 
						</a>
					</div>

					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Judul Pengumuman</th>
										<th>Tangal Pengumuman</th>
										<th>Isi Pengumuman</th>
										<th>Gambar Pengumuman</th>
										<th>Opsi</th>
																			
								   </tr>
								</thead>
								
								<tbody>
									<?php
										$no = 1;
										foreach($database as $db) : ?>
											<tr>
												<td><?php echo $no; ?></td>
												<td><?php echo $db->judul_pengumuman; ?></td>
												<td><?php echo $db->tanggal_pengumuman; ?></td>
												<td><?php echo $db->isi_pengumuman; ?></td>
												<td><?php echo $db->gambar_pengumuman; ?></td>
	
												<td>
													<a href="<?php echo base_url('Pengumuman/hapusdata/'.$db->id_pengumuman); ?>" onclick="return confirm('Anda yakin hapus ?')"><button type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button></a>
												</td>
											</tr>
									<?php
										$no++;
										endforeach;
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
