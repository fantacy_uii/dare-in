<?php
	defined('BASEPATH') OR exit('Akses langsung tidak diperbolehkan');
	//echo validation_errors();
?>

<section class="container-fluid">
	<div class="row">
		<div class="form-input clearfix">
			<div class="col-md-12">
				
				<?php
					if(isset($_SESSION['input_sukses']))
					{
				?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  	<strong>Sukses!</strong> <?php echo $_SESSION['input_sukses']; ?>
						</div>
				<?php
					}
				?>

				<div class="panel panel-primary">
					<div class="panel-heading">Tambah Data Posko</div>
					<div class="panel-body">
						
						<?php echo form_open('Posko/tambahposko', ['class' => 'form-horizontal', 'method' => 'post']); ?>

							<div class="form-group">
								<label for="nama_posko" class="control-label col-sm-2">Nama Posko </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="nama_posko" value="<?php echo set_value('nama_posko'); ?>">
									<?php echo form_error('nama_posko'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id_team" class="control-label col-sm-2">ID Team </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="id_team" value="<?php echo set_value('id_team'); ?>">
									<?php echo form_error('id_team'); ?>
								</div>
							</div>

							<div class="form-group">
								<label for="regional" class="control-label col-sm-2">Regional </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="regional" value="<?php echo set_value('regional'); ?>">
									<?php echo form_error('regional'); ?>
								</div>
							</div>

							<div class="form-group">
								<div class="btn-form col-sm-12">
									<a href="<?php echo base_url('Posko/lihatdata'); ?>"><button type="button" class='btn btn-default'>Batal</button></a>
									<button type="submit" class='btn btn-primary'>Simpan</button>
								</div>
							</div>
						<?php echo form_close(); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>