<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Posko</div>
				<div class="panel-body">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Posko</th>
										<th>ID Team</th>
										<th>Regional</th>															
								   </tr>
								</thead>
								
								<tbody>
									<?php
										$no = 1;
										foreach($database as $db) : ?>
											<tr>
												<td><?php echo $no; ?></td>
												<td><?php echo $db->nama_posko; ?></td>
												<td><?php echo $db->id_team; ?></td>
												<td><?php echo $db->regional; ?></td>
											</tr>
									<?php
										$no++;
										endforeach;
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
