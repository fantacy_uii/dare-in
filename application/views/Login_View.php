<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login - Sistem Pendataan Relawan Terintegrasi</title>

	<!-- Bootstrap core CSS -->
    <link href="<?php echo base_url().'assets/bootstrap/css/bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/login_source/login.css'?>" rel="stylesheet">
    

</head>
<body>
	<div class="container">
        <div align="center" style="margin-top: 210px;"> 
        <div class="col-md-4 col-md-offset-4">
            
          <form class="" action="<?php echo base_url().'Login/auth'?>" method="post">
            <h2 class="form-signin-heading">SIGN IN</h2>
            <?php echo $this->session->flashdata('msg');?>
            <label for="username" class="sr-only">Username</label>
            <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus>
            <label for="password" class="sr-only">Password</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
            <div class="checkbox">
              <p>
              <label>
                <input type="checkbox" value="remember-me">Remember me 
              </label>
              </p>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
          </form>
          <br>
          <a href="<?php echo base_url(); ?>">
              <button type="button" class="btn btn-lg btn-primary btn-block"><span class="glyphicon glyphicon-back"></span> Halaman Utama</button> 
            </a>
        </div>
        </div>

        </div> <!-- /container -->
 
 
    <!-- jQuery-->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
    <!-- Bootsrap -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
</body>

</html>