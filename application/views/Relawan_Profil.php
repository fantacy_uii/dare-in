<?php
	defined('BASEPATH') OR exit('Akses langsung tidak diperbolehkan');
	//echo validation_errors();
?>

<section class="container-fluid">
	<div class="row">
		<div class="form-input clearfix">
			<div class="col-md-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Profil</div>
					<div class="panel-body">
						
							<div class="form-group ">
								<label for="id_relawan" class="control-label col-sm-2">ID Relawan </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_id');?>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label for="id_relawan" class="control-label col-sm-2">NIK </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_NIK');?>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label  class="control-label col-sm-2">Nama </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_nama');?>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label  class="control-label col-sm-2">TTL </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_tanggal_lahir');?>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label  class="control-label col-sm-2">Gender </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_jenis_kelamin');?>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label class="control-label col-sm-2">Alamat </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_alamat');?>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label class="control-label col-sm-2">Pekerjaan </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_pekerjaan');?>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label class="control-label col-sm-2">Jenis </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_jenis_relawan');?>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label class="control-label col-sm-2">Keahlian </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_keahlian');?>
									</div>
								</div>
							</div>


							<div class="form-group ">
								<label class="control-label col-sm-2">ID Team </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_id_team');?>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<label class="control-label col-sm-2">ID Posko </label>
								<div class="col-sm-10">
									<div class="form-control">
										<?php echo $this->session->userdata('ses_id_posko');?>
									</div>
								</div>
							</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>