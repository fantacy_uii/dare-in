<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets_3/css/bootstrap.css'?>">

<section class="container-fluid">
	<div class="row">
		<div class="col-md-12">

			<?php
				if(isset($_SESSION['hapus_sukses']) || isset($_SESSION['update_sukses'])) :
					$notif = '';

					isset($_SESSION['hapus_sukses']) ? $notif .= $_SESSION['hapus_sukses'] : '';
					isset($_SESSION['update_sukses']) ? $notif .= $_SESSION['update_sukses'] : '';
			?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  	<strong>Sukses!</strong> <?php echo $notif; ?>
					</div>
			<?php
				endif;
			?>

			<div class="panel panel-primary">
				<div class="panel-heading">Pengumuman</div>
				<div class="panel-body">
					<div class="col-md-12" style="padding-bottom: 15px;">
						<a href="<?php echo base_url('Pengumuman/Adminlihatdata'); ?>">
							<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-back"></span> Data Pengumuman</button> 
						</a>

						<a href="<?php echo base_url('Pengumuman/lists_pengumuman'); ?>">
							<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-back"></span>Kembali</button> 
						</a>
					</div>

					<div class="col-md-12">
						<div class="table-responsive">
							<div class="container">
						<?php 
						$b=$data->row_array();
			?>
			<!DOCTYPE html>
			<html>
			<head>
				<title><?php echo $b['judul_pengumuman'];?></title>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets_3/css/bootstrap.css'?>">
			</head>
			<body>
				<div class="container">
					<div class="col-md-8 col-md-offset-2">
						<h2><?php echo $b['judul_pengumuman'];?></h2><hr/>
						<img src="<?php echo base_url().'assets_3/images/'.$b['gambar_pengumuman'];?>">
						<?php echo $b['isi_pengumuman'];?>
					</div>
					
				</div>

				<script src="<?php echo base_url().'assets_3/jquery/jquery-2.2.3.min.js'?>"></script>
				<script type="text/javascript" src="<?php echo base_url().'assets_3/js/bootstrap.js'?>"></script>
			</body>
			</html>
				</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script src="<?php echo base_url().'assets_3/jquery/jquery-2.2.3.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets_3/js/bootstrap.js'?>"></script>
