<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistem Pendataan Relawan Terintegrasi</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url().'assets/bootstrap/css/bootstrap.min.css'?>" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url().'assets/home_source/fontawesome-free/css/all.min.css'?>" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="<?php echo base_url().'assets/home_source/magnific-popup/magnific-popup.css'?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url().'assets/home_source/css/home.css'?>" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <STRONG>
        <a class="navbar-brand js-scroll-trigger" href="#page-top">DARE-IN</a>
        </STRONG>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#team">Team</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo base_url('Login/index') ?>">Sign In</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>Sistem Pendataan Relawan Terintegrasi</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class=" font-weight-bold">Sistem Pendataan Relawan Terintegrasi merupakan sebuah sistem yang dikembangkan untuk pendataan relawan berkompentensi di bidangnya</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Lebih Banyak</a>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">Sistem Pendataan Relawan Terintegrasi</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">Sistem ini merupakan suatu sistem yang dikembangkan untuk memudahkan BNPB dalam melakukan pendataan relawan berkompetensi di bidangnya. Relawan saat ini memang cukup banyak, tetapi relawan yang berkompetensi di bidangnya masih sangat sedikit. Jadi, di sistem ini akan memuat relawan yang berkompetensi yang mana akan sangat membantu dalam penanganan pada saat terjadi bencana. </p>
            <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Lebih Banyak</a>
          </div>
        </div>
      </div>
    </section>

    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Layanan</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-user-friends text-primary mb-3 sr-icon-1"></i>
              <h3 class="mb-3">TIM</h3>
              <p class="text-muted mb-0">Untuk melihat rekan relawan yang satu tim dengan anda.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-list-ul text-primary mb-3 sr-icon-2"></i>
              <h3 class="mb-3">POSKO</h3>
              <p class="text-muted mb-0">Berisi list posko - posko yang ada</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-bullhorn text-primary mb-3 sr-icon-4"></i>
              <h3 class="mb-3">PENGUMUMAN</h3>
              <p class="text-muted mb-0">Berisi pengumuman - pengumuman tentang kekurangan relawan di suatu daerah bencana yang memiliki keahlian tertentu</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-user-edit text-primary mb-3 sr-icon-3"></i>
              <h3 class="mb-3">EDIT</h3>
              <p class="text-muted mb-0">Juga bisa mengubah data, jikalau terdapat kesalahan atau kekurangan pada inputan data.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="col-lg-8 mx-auto text-center">
          <a class="btn btn-light btn-xl js-scroll-trigger" href="<?php echo base_url('Login') ?>">MASUK</a>
            <hr class="my-4">
          </div>

    <!-- Team -->
    <section class="bg-light" id="team">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">FANTACY</h2>
            <h3 class="section-subheading text-muted">INFORMATIKA UII 2017</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="assets/home_source/img/team/ibnu.jpg" alt="">
              <h4>Ibnu Haidar</h4>
              <p class="text-muted">17523130</p>
              <!--<p class="social-buttons"></p>-->
                  <a class="social-buttons" href="https://www.instagram.com/ibnuhaidarr/">
                    <h3 class="fab fa-instagram"></h3>
                  </a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="assets/home_source/img/team/tony.jpg" alt="">
              <h4>Hirzin Fathoni</h4>
              <p class="text-muted">17523128</p>
              <!--<p class="social-buttons"></p>-->
                  <a class="social-buttons" href="https://www.instagram.com/fathonihirzin/">
                    <h3 class="fab fa-instagram"></h3>
                  </a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="assets/home_source/img/team/rifqi.jpg" alt="">
              <h4>Rifqi Palisuri Palsam</h4>
              <p class="text-muted">17523120</p>
              <!--<p class="social-buttons"></p>-->
                  <a class="social-buttons" href="https://www.instagram.com/rifqipalisuri/">
                    <h3 class="fab fa-instagram"></h3>
                  </a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <p class="large text-muted">~ hanya sekumpulan orang yang baru belajar tentang dunia informatika ~</p>
          </div>
        </div>
      </div>
    </section>

    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Contact Us</h2>
            <hr class="my-4">
            <p class="mb-5">Jika punya pertanyaan, atau kritik maupun saran tentang sistem ini</p>
          </div>
        </div>
          <div class=" text-center">
            <i class="fas fa-envelope fa-3x mb-3 sr-contact-2"></i>
            <p>
              <a href="mailto:hirzinfathoni@gmail.com">admin@fantacy.com</a>
            </p>
          </div>
        </div>
      </div>
    </section>

    <div class="copyright py-4 text-center text-white">
      <div class="container">
        <small>Copyright &copy; fantacy 2018</small>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url().'assets/home_source/jquery/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.bundle.min.js'?>"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url().'assets/home_source/jquery-easing/jquery.easing.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/home_source/scrollreveal/scrollreveal.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/home_source/magnific-popup/jquery.magnific-popup.min.js'?>"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url().'assets/home_source/js/home.js'?>"></script>

  </body>

</html>
