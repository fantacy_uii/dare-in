<?php

class Relawan_Model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_all_data()
	{

		$query = $this->db->get('tb_relawan');
		return $query->result();
	}

	public function tambah_relawan()
	{
					$username = $this->input->post('username');
					$password = $this->input->post('password');
					$NIK = $this->input->post('NIK');
					$nama = $this->input->post('nama');
					$tanggal_lahir = $this->input->post('tanggal_lahir');
					$jenis_kelamin = $this->input->post('jenis_kelamin');
					$alamat = $this->input->post('alamat');
					$pekerjaan = $this->input->post('pekerjaan');
					$jenis_relawan = $this->input->post('jenis_relawan');
					$keahlian = $this->input->post('keahlian');
					$id_team = $this->input->post('id_team');
					$id_posko = $this->input->post('id_posko');

		$data = array (
					'username' => $username,
					'password' => md5($password),
					'NIK' => $NIK,
					'nama' => $nama,
					'tanggal_lahir' => $tanggal_lahir,
					'jenis_kelamin' => $jenis_kelamin,
					'alamat' => $alamat,
					'pekerjaan' => $pekerjaan,
					'jenis_relawan' => $jenis_relawan,
					'keahlian' => $keahlian,
					'id_team' => $id_team,
					'id_posko' => $id_posko
				);

		$this->db->insert('tb_relawan', $data);
	}

	public function edit_relawan($id)
	{
		$query = $this->db->get_where('tb_relawan', ['id_relawan' => $id]);
		return $query->row();
	}

	public function update_relawan()
	{
		$kondisi = ['id_relawan' => $this->input->post('id_relawan')];
		
		$data = [
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password'),
					'NIK' => $this->input->post('NIK'),
					'nama' => $this->input->post('nama'),
					'tanggal_lahir' => $this->input->post('tanggal_lahir'),
					'jenis_kelamin' => $this->input->post('jenis_kelamin'),
					'alamat' => $this->input->post('alamat'),
					'pekerjaan' => $this->input->post('pekerjaan'),
					'jenis_relawan' => $this->input->post('jenis_relawan'),
					'keahlian' => $this->input->post('keahlian'),
					'id_team' => $this->input->post('id_team'),
					'id_posko' => $this->input->post('id_posko')
				];

		$this->db->update('tb_relawan', $data, $kondisi);
	}

	public function hapus_relawan($id)
	{
		$this->db->delete('tb_relawan', ['id_relawan' => $id]);
	}
}

?>