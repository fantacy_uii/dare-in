<?php

class Riwayat_Model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_all_data()
	{
		$query = $this->db->get('tb_riwayat');
		return $query->result();
	}

	public function tambah_riwayat()
	{
		$data = [	'nama_riwayat' => $this->input->post('nama_riwayat'),
					'mulai_riwayat' => $this->input->post('mulai_riwayat'),
					'akhir_riwayat' => $this->input->post('akhir_riwayat'),
					'id_relawan' => $this->input->post('id_relawan'),
					'id_posko' => $this->input->post('id_posko'),

				];

		$this->db->insert('tb_riwayat', $data);
	}

	public function edit_riwayat($id)
	{
		$query = $this->db->get_where('tb_riwayat', ['id_riwayat' => $id]);
		return $query->row();
	}

	public function update_riwayat()
	{
		$kondisi = ['id_riwayat' => $this->input->post('id_riwayat')];
		
		$data = [
					'nama_riwayat' => $this->input->post('nama_riwayat'),
					'mulai_riwayat' => $this->input->post('mulai_riwayat'),
					'akhir_riwayat' => $this->input->post('akhir_riwayat'),
					'id_relawan' => $this->input->post('id_relawan'),
					'id_posko' => $this->input->post('id_posko'),
				];
		$this->db->update('tb_riwayat', $data, $kondisi);
	}

	public function hapus_riwayat($id)
	{
		$this->db->delete('tb_riwayat', ['id_riwayat' => $id]);
	}
}

?>