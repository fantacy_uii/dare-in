<?php

class Posko_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_all_data()
	{
		$query = $this->db->get('tb_posko');
		return $query->result();
	}

	public function tambah_posko()
	{
		$data = [
					'nama_posko' => $this->input->post('nama_posko'),
					'id_team' => $this->input->post('id_team'),
					'regional' => $this->input->post('regional')
				];

		$this->db->insert('tb_posko', $data);
	}

	public function edit_posko($id)
	{
		$query = $this->db->get_where('tb_posko', ['id_posko' => $id]);
		return $query->row();
	}

	public function update_posko()
	{
		$kondisi = ['id_posko' => $this->input->post('id_posko')];
		
		$data = [
					'nama_posko' => $this->input->post('nama_posko'),
					'id_team' => $this->input->post('id_team'),
					'regional' => $this->input->post('regional')
				];


		$this->db->update('tb_posko', $data, $kondisi);
	}

	public function hapus_posko($id)
	{
		$this->db->delete('tb_posko', ['id_posko' => $id]);
	}
}

?>