<?php

class Pengumuman_Model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_all_data()
	{
		$query = $this->db->get('tb_pengumuman');
		return $query->result();
	}

	public function edit_pengumuman($id)
	{
		$query = $this->db->get_where('tb_pengumuman', ['id_pengumuman' => $id]);
		return $query->row();
	}

	public function update_pengumuman()
	{
		$kondisi = ['id_pengumuman' => $this->input->post('id_pengumuman')];
		
		$data = [	'judul_pengumuman' => $this->input->post('judul_pengumuman'),
					'tanggal_pengumuman' => $this->input->post('tanggal_pengumuman'),
					'isi_pengumuman' => $this->input->post('isi_pengumuman'),
					'gambar_pengumuman' => $this->input->post('gambar_pengumuman')
				];
		$this->db->update('tb_pengumuman', $data, $kondisi);
	}

	public function hapus_pengumuman($id)
	{
		$this->db->delete('tb_pengumuman', ['id_pengumuman' => $id]);
	}

	function simpan_berita($judul_pengumuman,$tanggal_pengumuman,$isi_pengumuman,$gambar_pengumuman){
		$hsl=$this->db->query("INSERT INTO tb_pengumuman (judul_pengumuman,tanggal_pengumuman,isi_pengumuman,gambar_pengumuman) VALUES ('$judul_pengumuman','$tanggal_pengumuman','$isi_pengumuman','$gambar_pengumuman')");
		return $hsl;
	}

	function get_berita_by_kode($kode){
		$hsl=$this->db->query("SELECT * FROM tb_pengumuman WHERE id_pengumuman='$kode'");
		return $hsl;
	}

	function get_all_berita(){
		$hsl=$this->db->query("SELECT * FROM tb_pengumuman ORDER BY id_pengumuman DESC");
		return $hsl;
	}
}

?>