<?php

class Team_Model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_all_data()
	{
		$query = $this->db->get('tb_team');
		return $query->result();
	}

	public function tambah_tim()
	{
		$data = [
			'id_team' => $this->input->post('id_team'),
			'nama_team' => $this->input->post('nama_team'),
			'id_posko' => $this->input->post('id_posko'),
			'regional' => $this->input->post('regional')

		];

		$this->db->insert('tb_team', $data);
	}

	public function edit_tim($id)
	{
		$query = $this->db->get_where('tb_team', ['id_team' => $id]);
		return $query->row();
	}

	public function update_tim()
	{
		$kondisi = ['id_team' => $this->input->post('id_team')];
		
		$data = [
			'id_team' => $this->input->post('id_team'),
			'nama_team' => $this->input->post('nama_team'),
			'id_posko' => $this->input->post('id_posko'),
			'regional' => $this->input->post('regional')

		];
		$this->db->update('tb_team', $data, $kondisi);
	}

	public function hapus_tim($id)
	{
		$this->db->delete('tb_team', ['id_team' => $id]);
	}
}

?>