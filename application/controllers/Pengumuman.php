<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengumuman extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pengumuman_Model');
		$this->load->helper(['url_helper', 'form']);
		$this->load->library(['form_validation', 'session', 'upload']);
	}


	public function Adminlihatdata()
	{
		$data['database'] = $this->Pengumuman_Model->get_all_data();

		$data['title'] = "Pengumuman | Data";

		$this->load->view('templates/header', $data);
		$this->load->view('Pengumuman_Data', $data);
		$this->load->view('templates/footer');
	}

	public function RelawanPengumuman(){

	}

	public function formtambah()
	{
		$data['title'] = "Tambah Data | Pengumuman";

		$this->load->view('templates/header', $data);
		$this->load->view('Pengumuman_Form_Tambah');
		$this->load->view('templates/footer');
	}

	public function tambahpengumuman()
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formtambah();
		}
		else
		{
			$this->Pengumuman_Model->tambah_pengumuman();
			$this->session->set_flashdata('input_sukses','Data Pengumuman berhasil di input');
			redirect('/Pengumuman/Adminlihatdata');
		}
	}

	public function hapusdata($id)
	{
		$this->Pengumuman_Model->hapus_pengumuman($id);
		$this->session->set_flashdata('hapus_sukses','Data Pengumuman berhasil di hapus');
		redirect('/Pengumuman/Adminlihatdata');
	}

	public function formedit($id)
	{
		$data['title'] = 'Edit Data | Pengumuman';

		$data['db'] = $this->Pengumuman_Model->edit_pengumuman($id);

		$this->load->view('templates/header', $data);
		$this->load->view('Pengumuman_Form_Edit', $data);
		$this->load->view('templates/footer');
	}

	public function updatepengumuman($id)
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formedit($id);
		}
		else
		{
			$this->Pengumuman_Model->update_Pengumuman();
			$this->session->set_flashdata('update_sukses', 'Data Pengumuman berhasil diperbaharui');
			redirect('/Pengumuman/Adminlihatdata');
		}
	}

	public function validasi()
	{
		$this->form_validation->set_message('required', '{field} tidak boleh kosong');

		$config = [[
			'field' => 'judul_pengumuman',
			'label' => 'judul_pengumuman',
			'rules' => 'required'
		],
		[
			'field' => 'tanggal_pengumuman',
			'label' => 'tanggal_pengumuman',
			'rules' => 'required'

		],
		[
			'field' => 'isi_pengumuman',
			'label' => 'isi_pengumuman',
			'rules' => 'required'
		]];

		$this->form_validation->set_rules($config);
	}

	public function formpost()
	{
		$data['title'] = "Post Pengumuman";

		$this->load->view('templates/header', $data);
		$this->load->view('Pengumuman_Form_Post');
		$this->load->view('templates/footer');
	}

	function post_pengumuman(){
		$config['upload_path'] = './assets_3/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	    $this->upload->initialize($config);
	    if(!empty($_FILES['gambar_pengumuman']['name'])){
	        if ($this->upload->do_upload('gambar_pengumuman')){
	        	$gambar_pengumuman = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets_3/images/'.$gambar_pengumuman['file_name'];
	            $config['create_thumb']= FALSE;
	            $config['maintain_ratio']= FALSE;
	            $config['quality']= '100%';
	            $config['width']= 420;
	            $config['height']= 300;
	            $config['new_image']= './assets_3/images/'.$gambar_pengumuman['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar_pengumuman=$gambar_pengumuman['file_name'];
                $judul_pengumuman=$this->input->post('judul_pengumuman');
                $isi_pengumuman=$this->input->post('isi_pengumuman');
                $tanggal_pengumuman=$this->input->post('tanggal_pengumuman');

				$this->Pengumuman_Model->simpan_berita($judul_pengumuman,$tanggal_pengumuman,$isi_pengumuman,$gambar_pengumuman);
				redirect('Pengumuman/Adminlihatdata');
		}else{
			redirect('Pengumuman/Adminlihatdata');
	    }
	                 
	    }else{
			redirect('Pengumuman/Adminlihatdata');
		}			
	}

	function lists_pengumuman(){
		$x['data']=$this->Pengumuman_Model->get_all_berita();
		$this->load->view('Pengumuman_Post_List',$x);
	}

	function view_pengumuman(){
		$kode=$this->uri->segment(3);
		$x['data']=$this->Pengumuman_Model->get_berita_by_kode($kode);
		$this->load->view('Pengumuman_Post_View',$x);
	}

	function feed(){
		$x['data']=$this->Pengumuman_Model->get_all_berita();
		$this->load->view('Pengumuman_User',$x);
	}

	function view_feed(){
		$kode=$this->uri->segment(3);
		$x['data']=$this->Pengumuman_Model->get_berita_by_kode($kode);
		$this->load->view('Pengumuman_User_View',$x);
	}
}
?>