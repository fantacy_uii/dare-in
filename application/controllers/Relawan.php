<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Relawan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Relawan_Model');
		$this->load->helper(['url_helper', 'form']);
    	$this->load->library(['form_validation', 'session']);
	}

	public function lihatdata()
	{
		$data['database'] = $this->Relawan_Model->get_all_data();

		$data['title'] = "ADMIN | Data Relawan";

		$this->load->view('templates/header', $data);
		$this->load->view('Relawan_Data', $data);
		$this->load->view('templates/footer');
	}

	public function formtambah()
	{
		$data['title'] = "Tambah Data | Relawan";

		$this->load->view('templates/header', $data);
		$this->load->view('Relawan_Form_Tambah');
		$this->load->view('templates/footer');
	}

	public function tambahrelawan()
	{
		$this->form_validation->set_message('is_unique', '{field} sudah terpakai');

		$this->form_validation->set_rules('username', 'username', ['required', 'is_unique[relawan.username]']);

		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formtambah();
		}
		else
		{
			$this->Relawan_Model->tambah_relawan();
			$this->session->set_flashdata('input_sukses','Data Relawan berhasil di input');
			redirect('/Relawan/lihatdata');
		}
	}

	public function hapusdata($id)
	{
		$this->Relawan_Model->hapus_relawan($id);
		$this->session->set_flashdata('hapus_sukses','Data Relawan berhasil di hapus');
		redirect('/Relawan/lihatdata');
	}

	public function formedit($id)
	{
		$data['title'] = 'Edit Data | Relawan';

		$data['db'] = $this->Relawan_Model->edit_relawan($id);

		$this->load->view('templates/header', $data);
		$this->load->view('Relawan_Form_Edit', $data);
		$this->load->view('templates/footer');
	}

	public function updaterelawan($id)
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formedit($id);
		}
		else
		{
			$this->Relawan_Model->update_relawan();
			$this->session->set_flashdata('update_sukses', 'Data Relawan berhasil diperbaharui');
			redirect('/Relawan/lihatdata');
		}
	}

	public function validasi()
	{
		$this->form_validation->set_message('required', '{field} tidak boleh kosong');

		$config = [[
					'field' => 'username',
					'label' => 'username',
					'rules' => 'required'
				],
				[
					'field' => 'password',
					'label' => 'password',
					'rules' => 'required'
				],
				[
					'field' => 'id_team',
					'label' => 'id_team',
					'rules' => 'required'
				],
				[
					'field' => 'id_posko',
					'label' => 'id_posko',
					'rules' => 'required'
				]];

		$this->form_validation->set_rules($config);
	}

	public function profil(){
		$data['database'] = $this->Relawan_Model->get_all_data();

		$data['title'] = "ADMIN | Data Relawan";

		$this->load->view('templates/header', $data);
		$this->load->view('Relawan_Profil', $data);
		$this->load->view('templates/footer');
	}
}
?>