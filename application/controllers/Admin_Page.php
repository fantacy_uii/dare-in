<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Page extends CI_Controller {

	function __construct(){
    parent::__construct();
    //validasi jika user belum login
    if($this->session->userdata('masuk') != TRUE){
			redirect(base_url('Login'));
		}
  }
	
	public function index()
	{
		$this->load->view('Admin_Page_View');
	}
}