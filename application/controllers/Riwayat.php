<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Riwayat_Model');
		$this->load->helper(['url_helper', 'form']);
		$this->load->library(['form_validation', 'session']);
	}


	public function lihatdata()
	{
		$data['database'] = $this->Riwayat_Model->get_all_data();

		$data['title'] = " Data Riwayat Aktivitas";

		$this->load->view('templates/header', $data);
		$this->load->view('Riwayat_Data', $data);
		$this->load->view('templates/footer');
	}

	public function formtambah()
	{
		$data['title'] = "Tambah Data | Riwayat Aktivitas";

		$this->load->view('templates/header', $data);
		$this->load->view('Riwayat_Form_Tambah');
		$this->load->view('templates/footer');
	}

	public function tambahriwayat()
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formtambah();
		}
		else
		{
			$this->Riwayat_Model->tambah_riwayat();
			$this->session->set_flashdata('input_sukses','Data Riwayat Aktivitas berhasil di input');
			redirect('/Riwayat/lihatdata');
		}
	}

	public function hapusdata($id)
	{
		$this->Riwayat_Model->hapus_riwayat($id);
		$this->session->set_flashdata('hapus_sukses','Data Riwayat Aktivitas berhasil di hapus');
		redirect('/Riwayat/lihatdata');
	}

	public function formedit($id)
	{
		$data['title'] = 'Edit Data | Riwayat Aktivitas';

		$data['db'] = $this->Riwayat_Model->edit_riwayat($id);

		$this->load->view('templates/header', $data);
		$this->load->view('Riwayat_Form_Edit', $data);
		$this->load->view('templates/footer');
	}

	public function updateriwayat($id)
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formedit($id);
		}
		else
		{
			$this->Riwayat_Model->update_Riwayat();
			$this->session->set_flashdata('update_sukses', 'Data Riwayat Aktivitas berhasil diperbaharui');
			redirect('/Riwayat/lihatdata');
		}
	}

	public function validasi()
	{
		$this->form_validation->set_message('required', '{field} tidak boleh kosong');

		$config = [
		[
			'field' => 'nama_riwayat',
			'label' => 'nama_riwayat',
			'rules' => 'required'
		],
		[
			'field' => 'mulai_riwayat',
			'label' => 'mulai_riwayat',
			'rules' => 'required'


		],
		[
			'field' => 'akhir_riwayat',
			'label' => 'akhir_riwayat',
			'rules' => 'required'


		],
		[
			'field' => 'id_relawan',
			'label' => 'id_relawan',
			'rules' => 'required'


		],
		[
			'field' => 'id_posko',
			'label' => 'id_posko',
			'rules' => 'required'


		]];

		$this->form_validation->set_rules($config);
	}
}
?>