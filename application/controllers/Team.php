<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Team extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Team_Model');
		$this->load->helper(['url_helper', 'form']);
		$this->load->library(['form_validation', 'session']);
	}


	public function lihatdata()
	{
		$data['database'] = $this->Team_Model->get_all_data();

		$data['title'] = "Data Tim";

		$this->load->view('templates/header', $data);
		$this->load->view('Team_Data', $data);
		$this->load->view('templates/footer');
	}

	public function list()
	{
		$data['database'] = $this->Team_Model->get_all_data();

		$data['title'] = "Tim";

		$this->load->view('templates/header', $data);
		$this->load->view('Tim', $data);
		$this->load->view('templates/footer');
	}

	public function formtambah()
	{
		$data['title'] = "Tambah Data | Tim";

		$this->load->view('templates/header', $data);
		$this->load->view('Team_Form_Tambah');
		$this->load->view('templates/footer');
	}

	public function tambahtim()
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formtambah();
		}
		else
		{
			$this->Team_Model->tambah_tim();
			$this->session->set_flashdata('input_sukses','Data Tim berhasil di input');
			redirect('/Team/lihatdata');
		}
	}

	public function hapusdata($id)
	{
		$this->Team_Model->hapus_tim($id);
		$this->session->set_flashdata('hapus_sukses','Data Tim berhasil di hapus');
		redirect('/Team/lihatdata');
	}

	public function formedit($id)
	{
		$data['title'] = 'Edit Data | Tim';

		$data['db'] = $this->Team_Model->edit_tim($id);

		$this->load->view('templates/header', $data);
		$this->load->view('Team_Form_Edit', $data);
		$this->load->view('templates/footer');
	}

	public function updatetim($id)
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formedit($id);
		}
		else
		{
			$this->Team_Model->update_Tim();
			$this->session->set_flashdata('update_sukses', 'Data Tim berhasil diperbaharui');
			redirect('/Team/lihatdata');
		}
	}

	public function validasi()
	{
		$this->form_validation->set_message('required', '{field} tidak boleh kosong');

		$config = [[
			'field' => 'nama_team',
			'label' => 'nama_team',
			'rules' => 'required'
		],
		[
			'field' => 'id_posko',
			'label' => 'id_posko',
			'rules' => 'required'
		],
		[
			'field' => 'regional',
			'label' => 'regional',
			'rules' => 'required'

		]];

		$this->form_validation->set_rules($config);
	}

	public function tim()
	{
		$data['database'] = $this->Team_Model->get_all_data();

		$data['title'] = "Tim";

		$this->load->view('templates/header', $data);
		$this->load->view('Tim', $data);
		$this->load->view('templates/footer');
	}
}
?>