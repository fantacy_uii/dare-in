<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('Login_Model');
    }
	
	public function index()
	{
		$this->load->view('Login_View');
	}

	function auth(){
		$username=htmlspecialchars($this->input->post('username', TRUE), ENT_QUOTES);
		$password=htmlspecialchars($this->input->post('password', TRUE), ENT_QUOTES);

		$cek_admin=$this->Login_Model->auth_admin($username, $password);

		if($cek_admin->num_rows() > 0){//Jika login sebagai admin
			$data=$cek_admin->row_array();
			$this->session->set_userdata('masuk', TRUE);
			$data['level']=='1';
			$this->session->set_userdata('akses', '1');
			$this->session->set_userdata('ses_id', $data['id_admin']);
			$this->session->set_userdata('ses_nama', $data['nama']);
			redirect(base_url('Admin_Page'));
		}else{//jika login sebagai relawan
			$cek_relawan=$this->Login_Model->auth_relawan($username, $password);
			if($cek_relawan->num_rows() > 0){
				$data=$cek_relawan->row_array();
				$this->session->set_userdata('masuk', TRUE);
				$data['level']=='2';
				$this->session->set_userdata('akses', '2');
				$this->session->set_userdata('ses_id', $data['id_relawan']);
				$this->session->set_userdata('ses_NIK', $data['NIK']);
				$this->session->set_userdata('ses_nama', $data['nama']);
				$this->session->set_userdata('ses_tanggal_lahir', $data['tanggal_lahir']);
				$this->session->set_userdata('ses_jenis_kelamin', $data['jenis_kelamin']);
				$this->session->set_userdata('ses_alamat', $data['alamat']);
				$this->session->set_userdata('ses_pekerjaan', $data['pekerjaan']);
				$this->session->set_userdata('ses_jenis_relawan', $data['jenis_relawan']);
				$this->session->set_userdata('ses_keahlian', $data['keahlian']);
				$this->session->set_userdata('ses_id_team', $data['id_team']);
				$this->session->set_userdata('ses_id_posko', $data['id_posko']);
				redirect(base_url('Relawan_Page'));
			}else{//jika username dan password tidak ditemukan atau salah
				echo $this->session->set_flashdata('msg', 'Username atau Password Salah');
				redirect(base_url('Login'));
			}
		}		
		}

	function logout(){
	$this->session->sess_destroy();
	redirect(base_url('Login'));
	}

}