<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Posko extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Posko_Model');
		$this->load->helper(['url_helper', 'form']);
		$this->load->library(['form_validation', 'session']);
	}


	public function lihatdata()
	{
		$data['database'] = $this->Posko_Model->get_all_data();

		$data['title'] = " Data Posko";

		$this->load->view('templates/header', $data);
		$this->load->view('Posko_Data', $data);
		$this->load->view('templates/footer');
	}

	public function formtambah()
	{
		$data['title'] = "Tambah Data | Posko";

		$this->load->view('templates/header', $data);
		$this->load->view('Posko_Form_Tambah');
		$this->load->view('templates/footer');
	}

	public function tambahposko()
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formtambah();
		}
		else
		{
			$this->Posko_Model->tambah_posko();
			$this->session->set_flashdata('input_sukses','Data mobil berhasil di input');
			redirect('/Posko/lihatdata');
		}
	}

	public function hapusdata($id)
	{
		$this->Posko_Model->hapus_posko($id);
		$this->session->set_flashdata('hapus_sukses','Data Posko berhasil di hapus');
		redirect('/Posko/lihatdata');
	}

	public function formedit($id)
	{
		$data['title'] = 'Edit Data | Posko';

		$data['db'] = $this->Posko_Model->edit_posko($id);

		$this->load->view('templates/header', $data);
		$this->load->view('Posko_Form_Edit', $data);
		$this->load->view('templates/footer');
	}

	public function updateposko($id)
	{
		$this->validasi();

		if($this->form_validation->run() === FALSE)
		{
			$this->formedit($id);
		}
		else
		{
			$this->Posko_Model->update_Posko();
			$this->session->set_flashdata('update_sukses', 'Data Posko berhasil diperbaharui');
			redirect('/Posko/lihatdata');
		}
	}

	public function validasi()
	{
		$this->form_validation->set_message('required', '{field} tidak boleh kosong');

		$config = [[
			'field' => 'nama_posko',
			'label' => 'nama_posko',
			'rules' => 'required'
		],
		[
			'field' => 'regional',
			'label' => 'regional',
			'rules' => 'required'
		]];

		$this->form_validation->set_rules($config);
	}

	public function list()
	{
		$data['database'] = $this->Posko_Model->get_all_data();

		$data['title'] = "Posko";

		$this->load->view('templates/header', $data);
		$this->load->view('Posko_List', $data);
		$this->load->view('templates/footer');
	}
}
?>