-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2018 at 06:02 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_relawan`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(6) NOT NULL,
  `username` varchar(12) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `username`, `nama`, `password`, `level`) VALUES
(1, 'admin1', 'Hirzin Fathoni', 'cb9f2c4f4c7f4ade4b851482af9fd547', '1'),
(2, 'admin2', 'Ibnu Haidar', 'a39cddeb3bc6222a5f8ca9baec4a26f9', '1'),
(3, 'admin3', 'Rifqi Palisuri Palsam', '413e94976c2abfabe0c04769095f6111', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman`
--

CREATE TABLE `tb_pengumuman` (
  `id_pengumuman` int(6) NOT NULL,
  `judul_pengumuman` varchar(50) NOT NULL,
  `tanggal_pengumuman` date NOT NULL,
  `isi_pengumuman` text NOT NULL,
  `gambar_pengumuman` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengumuman`
--

INSERT INTO `tb_pengumuman` (`id_pengumuman`, `judul_pengumuman`, `tanggal_pengumuman`, `isi_pengumuman`, `gambar_pengumuman`) VALUES
(7, 'Kekurangan Dokter', '2018-11-28', '<p>Dibutuhkan dokter dengan keahlian PPGD di Posko JOGJA2</p>\r\n', 'b8671561acf3de1df7586b98f90b70a9.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_posko`
--

CREATE TABLE `tb_posko` (
  `id_posko` int(12) NOT NULL,
  `nama_posko` varchar(50) NOT NULL,
  `id_team` int(12) NOT NULL,
  `regional` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_posko`
--

INSERT INTO `tb_posko` (`id_posko`, `nama_posko`, `id_team`, `regional`) VALUES
(1, 'Jogja1', 123, 'Yogyakarta'),
(2, 'Jogja2', 142, 'Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `tb_relawan`
--

CREATE TABLE `tb_relawan` (
  `id_relawan` int(6) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('1','2') NOT NULL,
  `NIK` int(16) NOT NULL,
  `nama` varchar(80) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` text NOT NULL,
  `alamat` text NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `jenis_relawan` varchar(50) NOT NULL,
  `keahlian` varchar(50) NOT NULL,
  `id_team` int(12) NOT NULL,
  `id_posko` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_relawan`
--

INSERT INTO `tb_relawan` (`id_relawan`, `username`, `password`, `level`, `NIK`, `nama`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `pekerjaan`, `jenis_relawan`, `keahlian`, `id_team`, `id_posko`) VALUES
(1, 'tony3212', '6f6b62d5c4aeca6e328ad11b2d9bc6e5', '1', 2147483647, 'Hirzin Fathoni', '1998-11-28', 'laki-laki', 'Condong Catur', 'Dokter', 'Dokter', 'Dokter-PPGD', 142, 442),
(2, 'ibnu2', '195ace8d50de761419faf08845304398', '1', 2147483647, 'Ibnu Haidar', '1999-12-31', 'laki-laki', 'Yogyakarta', 'Perawat', 'Perawat', 'Perawat-BTLS', 123, 432),
(3, 'palsam213', 'cf79fcd85712af69117200a72cbdc989', '1', 2147483647, 'Rifqi Palisrui Palsam', '1998-03-12', 'laki-laki', 'Jakal', 'Apoteker', 'Apoteker', '-', 123, 444),
(4, 'fadel', 'ceb63ef144ba1f8bcd95432eab904397', '1', 0, '', '0000-00-00', 'laki-laki', '', '', 'Dokter', '-', 123, 442);

-- --------------------------------------------------------

--
-- Table structure for table `tb_riwayat`
--

CREATE TABLE `tb_riwayat` (
  `id_riwayat` int(6) NOT NULL,
  `nama_riwayat` varchar(50) NOT NULL,
  `mulai_riwayat` date NOT NULL,
  `akhir_riwayat` date NOT NULL,
  `id_relawan` int(6) NOT NULL,
  `id_posko` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_riwayat`
--

INSERT INTO `tb_riwayat` (`id_riwayat`, `nama_riwayat`, `mulai_riwayat`, `akhir_riwayat`, `id_relawan`, `id_posko`) VALUES
(1, '0', '0033-02-01', '0044-02-01', 34, 55);

-- --------------------------------------------------------

--
-- Table structure for table `tb_team`
--

CREATE TABLE `tb_team` (
  `id_team` int(12) NOT NULL,
  `nama_team` varchar(50) NOT NULL,
  `id_posko` int(12) NOT NULL,
  `regional` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_team`
--

INSERT INTO `tb_team` (`id_team`, `nama_team`, `id_posko`, `regional`) VALUES
(123, 'JOGJA1', 221312, 'Yogyakarta'),
(142, 'JOGJA2', 1, 'Yogyakarta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `tb_posko`
--
ALTER TABLE `tb_posko`
  ADD PRIMARY KEY (`id_posko`),
  ADD KEY `id_team` (`id_team`);

--
-- Indexes for table `tb_relawan`
--
ALTER TABLE `tb_relawan`
  ADD PRIMARY KEY (`id_relawan`),
  ADD KEY `team` (`id_team`),
  ADD KEY `posko` (`id_posko`);

--
-- Indexes for table `tb_riwayat`
--
ALTER TABLE `tb_riwayat`
  ADD PRIMARY KEY (`id_riwayat`),
  ADD KEY `id_relawan` (`id_relawan`),
  ADD KEY `id_posko` (`id_posko`);

--
-- Indexes for table `tb_team`
--
ALTER TABLE `tb_team`
  ADD PRIMARY KEY (`id_team`),
  ADD KEY `id_posko` (`id_posko`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  MODIFY `id_pengumuman` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_posko`
--
ALTER TABLE `tb_posko`
  MODIFY `id_posko` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_relawan`
--
ALTER TABLE `tb_relawan`
  MODIFY `id_relawan` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_riwayat`
--
ALTER TABLE `tb_riwayat`
  MODIFY `id_riwayat` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_team`
--
ALTER TABLE `tb_team`
  MODIFY `id_team` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
